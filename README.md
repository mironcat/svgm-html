# SVGm-html
## Getting Started
```
git clone
```
## Documentation
[link]

## Release History
_0.0.3_ - re-writed File Loader by promise
_0.0.2_ - buttons fixed
_0.0.1_ - beta

## License
Copyright (c) 2018 mironcat  
Licensed under the MIT license.

[link]: https://bitbucket.org/mironcat/svgm-html/src/master/docs
