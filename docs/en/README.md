## Introduction
> **[SVGm.cf]** - service for semi-automatic measurement of two-dimensional distances and area of vector elements from [SVG] files generated vy [Inkscape].

### Features
<!-- - Фотографии не требуют подготовки. Можно выполнять измерения объектов, которые сложно отделить от фона. Например, такими объектами являются отпечатки ископаемых растений.
- Легко вернуться к исходным измерениям для проверки или их корректировки. SVG-файлы имеют небольшой размер. 
- Относительная простота в освоении. Большинство специализированных программ требуют специальных знаний для подготовки изображений для автоматического измерения или освоения сложного интерфейса. 
- Описательные возможности. В Inkscape для каждого объекта доступны два поля title и description, которые можно использовать для добавления дополнительной информации, комментариев и т.д. -->
 
### Requiments
1. free and open-source vector graphics editor [Inkscape](https://inkscape.org/ru/download/) on your computer
2. access to page of [SVGm.cf]

## Quick start
+ **Prepare SVG-file:**
	- Run Inkscape, create new file and import an image 
	- Set the scalebar using the "Bezier curves" tool. Change label of the scalbar-path to 'scalebar' with value. Example: `scalebar=3` or `scalebar=0.5`
	- Draw measuring elements using **Bezier curves** ![draw_pen](../images/draw_pen.png), **rectangle** or **ellipse**
	- Group all measures that relate to one object and his scalebar.
+ **Run file at [SVGm.cf]**
	- Save and upload the svg-file on the page of [SVGm.cf] and press RUN-button!
+ **Экспорт полученных данных**
	- button `Copy to Clipboard`
	- button `Download .csv` - download the data as .csv file
	- button `Clear` - clear screen

## Version History
- _0.0.5_ - docs
- _0.0.1_ - beta-version

## License
Copyright (c) 2018 [mironcat]
Licensed under the [MIT] license.

## Full documentaion
[link](full.md)

[Inkscape]: https://inkscape.org/release/
[SVGm.cf]: https://svgm.cf
[SVG]: https://inkscape.org/develop/about-svg/
[MIT]: https://opensource.org/licenses/MIT
[mironcat]: https://github.com/mironcat/svgm

