let SVGtext ='<?xml version="1.0" encoding="UTF-8" standalone="no"?><!-- Created with Inkscape (http://www.inkscape.org/) -->  <svg    xmlns:dc="http://purl.org/dc/elements/1.1/"    xmlns:cc="http://creativecommons.org/ns#"    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"    xmlns:svg="http://www.w3.org/2000/svg"    xmlns="http://www.w3.org/2000/svg"    xmlns:xlink="http://www.w3.org/1999/xlink"    xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"    xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"    width="210mm"    height="297mm"    viewBox="0 0 210 297"    version="1.1"    id="svg8"    inkscape:version="0.92.3 (2405546, 2018-03-11)"    sodipodi:docname="minimum_test.svg">   <defs      id="defs2">     <inkscape:path-effect        effect="bspline"        id="path-effect48"        is_visible="true"        weight="33.333333"        steps="2"        helper_size="0"        apply_no_weight="true"        apply_with_weight="true"        only_selected="false" />     <inkscape:path-effect        effect="bspline"        id="path-effect16"        is_visible="true"        weight="33.333333"        steps="2"        helper_size="0"        apply_no_weight="true"        apply_with_weight="true"        only_selected="false" />     <inkscape:path-effect        effect="bspline"        id="path-effect12"        is_visible="true"        weight="33.333333"        steps="2"        helper_size="0"        apply_no_weight="true"        apply_with_weight="true"        only_selected="false" />     <inkscape:path-effect        effect="bspline"        id="path-effect48-1"        is_visible="true"        weight="33.333333"        steps="2"        helper_size="0"        apply_no_weight="true"        apply_with_weight="true"        only_selected="false" />     <inkscape:path-effect        effect="bspline"        id="path-effect48-1-4"        is_visible="true"        weight="33.333333"        steps="2"        helper_size="0"        apply_no_weight="true"        apply_with_weight="true"        only_selected="false" />   </defs>   <sodipodi:namedview      id="base"      pagecolor="#ffffff"      bordercolor="#666666"      borderopacity="1.0"      inkscape:pageopacity="0.0"      inkscape:pageshadow="2"      inkscape:zoom="0.98994949"      inkscape:cx="332.21252"      inkscape:cy="821.65089"      inkscape:document-units="mm"      inkscape:current-layer="g183"      showgrid="false"      showguides="true"      inkscape:guide-bbox="true">     <sodipodi:guide        position="85.590356,216.33132"        orientation="0,1"        id="guide44"        inkscape:locked="false" />   </sodipodi:namedview>   <metadata      id="metadata5">     <rdf:RDF>       <cc:Work          rdf:about="">         <dc:format>image/svg+xml</dc:format>         <dc:type            rdf:resource="http://purl.org/dc/dcmitype/StillImage" />         <dc:title></dc:title>       </cc:Work>     </rdf:RDF>   </metadata>   <g      inkscape:label="Layer 1"      inkscape:groupmode="layer"      id="layer1">     <g        id="g102">       <image          style="stroke-width:7.06071329"          width="76.743896"          height="69.549156"          preserveAspectRatio="none"          xlink:href="file:///D:/ROOT/AeroFS/DB-Image/BRW/002/BRW-002-L_01.tif"          id="image30"          x="8.8464594"          y="11.119525"          sodipodi:absref="D:\ROOT\AeroFS\DB-Image\BRW\002\BRW-002-L_01.tif" />       <path          inkscape:label="scalebar=100"          inkscape:original-d="m 47.247024,69.470145 c 5.327366,-0.01207 10.654469,-0.02389 15.981306,-0.03544"          inkscape:path-effect="#path-effect48"          inkscape:connector-curvature="0"          id="path46"          d="m 47.247024,69.470145 c 5.327367,-0.01181 10.65447,-0.02363 15.981306,-0.03544"          style="fill:#00c0af;fill-opacity:1;stroke:#ff0000;stroke-width:0.30000001;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:markers stroke fill" />       <ellipse          inkscape:label="limb"          ry="24.284971"          rx="26.930803"          cy="40.448654"          cx="47.908485"          id="path68"          style="fill:none;fill-opacity:1;stroke:#ff0000;stroke-width:0.30000004;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:markers stroke fill">         <desc            id="desc187">b</desc>       </ellipse>     </g>     <g        id="g178">       <image          style="stroke-width:7.06071377"          width="76.743889"          height="69.549156"          preserveAspectRatio="none"          xlink:href="file:///D:/ROOT/AeroFS/DB-Image/BRW/003/BRW-003-P_01.tif"          id="image41"          x="97.402107"          y="11.119525"          sodipodi:absref="D:\ROOT\AeroFS\DB-Image\BRW\003\BRW-003-P_01.tif" />       <path          inkscape:label="scalebar=100"          inkscape:original-d="m 135.80035,69.237265 c 5.32737,-0.01207 10.65447,-0.02389 15.98131,-0.03544"          inkscape:path-effect="#path-effect48-1"          inkscape:connector-curvature="0"          id="path46-3"          d="m 135.80035,69.237265 c 5.32737,-0.01181 10.65447,-0.02363 15.98131,-0.03544"          style="fill:#00c0af;fill-opacity:1;stroke:#ff0000;stroke-width:0.30000001;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:markers stroke fill" />       <ellipse          inkscape:label="limb"          ry="27.497768"          rx="29.671131"          cy="39.976189"          cx="135.59895"          id="path68-0"          style="fill:none;fill-opacity:1;stroke:#ff0000;stroke-width:0.30000004;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:markers stroke fill">         <desc            id="desc185">a</desc>       </ellipse>     </g>     <g        id="g183">       <image          style="stroke-width:7.02722931"          width="77.109566"          height="69.880554"          preserveAspectRatio="none"          xlink:href="file:///D:/ROOT/AeroFS/DB-Image/BRW/004/BRW-004-L_01.tif"          id="image112"          x="8.8464594"          y="80.668678"          sodipodi:absref="D:\ROOT\AeroFS\DB-Image\BRW\004\BRW-004-L_01.tif" />       <path          inkscape:label="scalebar=100"          inkscape:original-d="m 47.503771,138.90353 c 5.32737,-0.0121 10.654472,-0.0239 15.981313,-0.0354"          inkscape:path-effect="#path-effect48-1-4"          inkscape:connector-curvature="0"          id="path46-3-1"          d="m 47.503771,138.90353 c 5.327371,-0.0118 10.654473,-0.0236 15.981313,-0.0354"          style="fill:#00c0af;fill-opacity:1;stroke:#ff0000;stroke-width:0.30000001;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:markers stroke fill" />       <ellipse          inkscape:label="limb"          ry="23.822813"          rx="26.797983"          cy="107.03656"          cx="46.834656"          id="path68-0-0"          style="fill:none;fill-opacity:1;stroke:#ff0000;stroke-width:0.30000004;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:markers stroke fill">         <desc            id="desc189">a</desc>       </ellipse>     </g>   </g> </svg> ';
"use strict";
function fileLoader() {
  return new Promise(function(resolve, reject) {
      let files = document.getElementById("files").files;
      if (!files.length) {
        alert('Please select a file!');
        const resultFromFile = svgm.prepareSVG( SVGtext, "default" );
        resolve(resultFromFile);
        return;
      }
    if (!files.length) return;
      let results =  [];
      for (var i = 0; i < files.length; i++) {
          var file = files[i];  
          let reader = new FileReader();
          let filename = file.name;
            reader.onload = function(evt) {
              console.log('filename='+'-'+filename);
              let rawSvg = evt.target.result;
              let resultFromFile = svgm.prepareSVG( rawSvg, filename );
              results.push(resultFromFile);
              if (results.length == files.length) {
                resolve(results[0]);
              } 
            };
          reader.readAsText(file);
      }//end for
  });

}
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
let lang = 'ru';
let translates = {
  trlist_smpl:['lang','run','clear','copyres','downloadcsv', 'mode', 'docs'],
  trlist_adv:['lang','run','clear','mode_adv', 'docs'],
  ru:{
    headings:["файл","объект", "тип", "Метка", "значение", "Название", "Описание", "комментарии"],
    buttons:{
      lang:"ru",
      run:"запуск",
      copyres:"в буфер обмена",
      downloadcsv:"скачать .csv",
      clear:"очистить",
      mode:"[ СТАНДАРТНЫЙ РЕЖИМ ]",
      mode_adv:"[ ПРОСТОЙ РЕЖИМ ]",
      docs:"[ СПРАВКА ]",
   
    },
    buttons_alt:{
      copyres:"скопировано", 
    },
    attention:{
      file:'Пожалуйста, выберите файл! (Будут загружены данные по умолчанию).'
    }
  },
  en:{
    headings:["filename","name", "type", "label", "value", "title", "description", "comments"],
    buttons:{
      lang:"en",      
      run:"run",
      copyres:"copy to clipboard",
      downloadcsv:"download .csv",
      clear:"clear",
      mode:"[   STANDART MODE   ]",
      mode_adv:"[    SIMPLE MODE    ]",
      docs:"[   DOCS   ]"
    },
    buttons_alt:{
      copyres:"copied", 
    }
  }
};
function changeLang(buttons) {
  if (lang!='ru' && lang!='en') lang='en';
  for (const key in buttons){
    const button=buttons[key];
    document.getElementById(button).innerHTML=translates[lang].buttons[button];
  } 
}