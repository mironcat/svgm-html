  /*
 * svgm-html
 * 
 * Copyright (c) 2018 mironcat
 * Licensed under the MIT license.
 */
 'use strict'
document.addEventListener("DOMContentLoaded", function(event) {
    //lang=document.cookie = "lang="+;
    lang = getCookie("lang");
    let buttons = ['lang','run','clear','copyres','downloadcsv'];
    buttons.forEach(b => {
      document.getElementById(b).addEventListener('click', function(evt) {clickButton[b]();}, false);     
    });
    changeLang(translates.trlist_smpl);
});
//-----------------------------------------------------------------------------
let dataTable =null;

function renderTable(result) {
  // body...
    const dataSet = {
      headings:translates[lang].headings,
      data: []
    };
      for (const row of result) {
        dataSet.data.push([
            row.filename,
            row.name,
            row.type,
            row.label,
            row.value,
            row.title,
            row.description,
            row.comments
          ]);
      };
  clickButton.clear();
  const resgrid = document.querySelector("#grid-with-results");  
  dataTable = new DataTable(resgrid, {
      data: dataSet,
      perPage: dataSet.data.length,
      perPageSelect: [5, 10, 15, dataSet.data.length]      
    });
   document.getElementById("clear").disabled=false;
  document.getElementById("downloadcsv").disabled=false;
  document.getElementById("copyres").disabled=false;   
}

const clickButton = {
  lang: function () {
      if (lang=="ru") lang="en"; else lang="ru";
      document.cookie = "lang="+lang;
      changeLang(translates.trlist_smpl);
  },
  run: function () {
     // this.clear();
      fileLoader().then(
          response => renderTable(response),
          error => alert(`Rejected: ${error}`)
        );
  },
  clear: function () {
    // body...
       dataTable = null;
       const grid = document.getElementById("grid");
       if(grid.innerHTML.length) grid.innerHTML="";
        let tablenode = document.createElement("TABLE"); 
        tablenode.setAttribute("id", "grid-with-results");
        grid.appendChild(tablenode);
      document.getElementById("copyres").innerHTML=translates[lang].buttons.copyres;
      document.getElementById("copyres").disabled=true;
      document.getElementById("downloadcsv").disabled=true;
      document.getElementById("clear").disabled=true;
      document.getElementById("editor").hidden = true;
  },
 copyres: function () {
      document.getElementById("editor").hidden = false;
      let csvres = dataTable.export({
        type: "csv",
        download: false,
        columnDelimiter:  "\t"
      });
      var copyText = document.getElementById("editor");
      copyText.value = csvres;
      copyText.select();
      document.execCommand("copy");
      document.getElementById("copyres").innerHTML=translates[lang].buttons_alt.copyres;
      document.getElementById("editor").hidden = true;
  },
  downloadcsv: function () {
        dataTable.export({
        type: "csv",
        filename: "results",
        download: true,
        columnDelimiter:  ","
      });
  }
}

