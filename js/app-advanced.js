  /*
 * svgm-html
 * https://github.com/mironcat/svgm
 *
 * Copyright (c) 2018 mironcat
 * Licensed under the MIT license.
 */
  'use strict'
document.addEventListener("DOMContentLoaded", function(event) {
    //lang=document.cookie = "lang="+;
    lang = getCookie("lang");
    ['lang','run','clear'].forEach(b => {
      document.getElementById(b).addEventListener('click', function(evt) {clickButton[b]();}, false);     
    });
    changeLang(translates.trlist_adv);
});
//----------------------------------------------------------------------------

const clickButton = {
  lang: function () {
      if (lang=="ru") lang="en"; else lang="ru";
      document.cookie = "lang="+lang;
      changeLang(translates.trlist_adv);
  },
  run: function () {
      fileLoader().then(
          response => renderTable(response),
          error => alert(`Rejected: ${error}`)
        );
  },
  clear: function () {
    // body...
     const grid = window.document.getElementById("grid");
     $("#gridPanel").hide();
     if(grid.innerHTML.length) grid.innerHTML="";
     window.document.getElementById("clear").disabled=true;
  }
}

function renderTable(result) {
      document.getElementById('gridPanel').style.visibility = "visible";
      $("#gridPanel").show();
      $("#grid").html("<p align='center' style='color:grey;'>(processing...)</p>");
      let renderers = $.extend(
          $.pivotUtilities.renderers,
       /*   $.pivotUtilities.plotly_renderers,
          $.pivotUtilities.d3_renderers,*/
          $.pivotUtilities.export_renderers
          );            
      const defaultSettings = {
                  renderers: renderers,
                  aggregatorName: "List Unique Values",
                  vals: ["value"],
                  cols: ["type"], rows: ["name", "label"],
                  rendererName: "Table"
              };                                 
      const dataSet = [translates.en.headings];
          for (const row of result) {
            dataSet.push([
                row.filename,
                row.name,
                row.type,
                row.label,
                row.value,
                row.title,
                row.description,
                row.comments
              ]);
          };
     $("#grid").pivotUI(dataSet, defaultSettings, true);      
       window.document.getElementById("clear").disabled=false;
   } // end render tables
